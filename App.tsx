import React, {FC, useEffect} from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {
  DarkTheme,
  DefaultTheme,
  NavigationContainer,
  useTheme,
} from '@react-navigation/native';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {Security} from './src/components/otherComponents/Security';
import Colors from './src/utils/Colors';
import Indicator from './src/components/appComponents/Indicator';
import RootNavigation from './src/navigations/RootNavigation';
import SplashScreen from 'react-native-splash-screen';
import Strings from './src/utils/Strings';
import * as HelperStyles from './src/utils/HelperStyles';
import * as Helpers from './src/utils/Helpers';

const App: FC = () => {
  // Configuration Variables
  // Dark Theme
  const customDarkTheme = {
    ...DarkTheme,
    colors: {
      ...DarkTheme.colors,
      background: Colors.black,
      primary: Colors.primary,
      text: Colors.white,
    },
  };

  // Light Theme
  const customLightTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: Colors.white,
      primary: Colors.primary,
      text: Colors.black,
    },
  };

  // Theme Variables
  const themeScheme = Helpers.getThemeScheme();
  const Theme = useTheme().colors;

  useEffect(() => {
    let isFocus: boolean = true;

    // For react-native-splash-screen configuration
    SplashScreen.hide();

    return (): void => {
      isFocus = false;
    };
  }, []);

  return (
    <GestureHandlerRootView style={HelperStyles.flex(1)}>
      <SafeAreaView style={HelperStyles.screenContainer(Theme.background)}>
        <StatusBar barStyle={'default'} backgroundColor={Theme.text} />

        <NavigationContainer
          fallback={<Indicator />}
          theme={
            themeScheme == Strings.dark ? customDarkTheme : customLightTheme
          }>
          <RootNavigation />
        </NavigationContainer>
      </SafeAreaView>
    </GestureHandlerRootView>
  );
};

export default Security(App);
