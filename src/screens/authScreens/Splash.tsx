import React, {FC, useCallback} from 'react';
import {Image, View} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import {useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import Strings from '../../utils/Strings';
import Styles from '../../styles/authStyles/Splash';
import * as HelperStyles from '../../utils/HelperStyles';

const Splash: FC = (props: any) => {
  // Splash Variables

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      init();

      return (): void => {
        isFocus = false;
      };
    }, []),
  );

  function init(): void {
    setTimeout((): void => {
      props.navigation.navigate(Strings.dashboard);
    }, 2500);
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={Styles.screenContainer}>
        <View style={HelperStyles.screenSubContainer}>
          <Image
            resizeMode={'contain'}
            source={Assets.logo}
            style={HelperStyles.imageView(160, 160)}
          />
        </View>
      </View>
    </View>
  );
};

export default Splash;
