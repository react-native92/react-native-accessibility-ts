import React, {FC, ReactElement, useCallback, useState} from 'react';
import {
  Alert,
  BackHandler,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Colors from '../../utils/Colors';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/Dashboard';
import * as HelperStyles from '../../utils/HelperStyles';

const Dashboard: FC = (props: any) => {
  // Dashboard Variables
  const [count, setCount] = useState<number>(0);

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      BackHandler.addEventListener('hardwareBackPress', backAction);

      return (): void => {
        isFocus = false;

        BackHandler.removeEventListener('hardwareBackPress', backAction);
      };
    }, []),
  );

  function backAction(): boolean {
    Alert.alert(Strings.exitAccount, Strings.exitAlertText, [
      {
        text: Strings.cancel,
        onPress: (): void => {},
        style: 'cancel',
      },
      {
        text: Strings.confirm,
        onPress: (): void => {
          BackHandler.exitApp();
        },
      },
    ]);

    return true;
  }

  function renderAccessible(): ReactElement {
    return (
      <View accessible={true} style={HelperStyles.padding(8, 8)}>
        <Text
          style={HelperStyles.textView(
            16,
            '600',
            Theme.text,
            'center',
            'none',
          )}>
          {Strings.accessible}
        </Text>
      </View>
    );
  }

  function renderAccessibilityLabel(): ReactElement {
    return (
      <View
        accessible={true}
        accessibilityLabel={`${Strings.accessibilityLabel} ${Strings.component}`}
        style={HelperStyles.padding(8, 8)}>
        <Text
          style={HelperStyles.textView(
            16,
            '600',
            Theme.text,
            'center',
            'none',
          )}>
          {Strings.accessibilityLabel}
        </Text>
      </View>
    );
  }

  function renderAccessibilityLabelledBy(): ReactElement {
    return (
      <View
        accessible={true}
        accessibilityLabel={`${Strings.accessibilityLabelledBy} ${Strings.component}`}
        style={HelperStyles.padding(8, 8)}>
        <Text
          accessible={true}
          accessibilityLabel={Strings.emailAddress}
          nativeID={'emailAddressID'}
          style={HelperStyles.textView(16, '600', Theme.text, 'left', 'none')}>
          {Strings.emailAddress}
        </Text>

        <View style={Styles.container}>
          <TextInput
            accessible={true}
            accessibilityLabelledBy={'emailAddressID'}
            placeholderTextColor={Colors.manatee}
            placeholder={Strings.textInput}
            style={[
              HelperStyles.justView('width', '100%'),
              HelperStyles.textView(14, '600', Colors.manatee, 'left', 'none'),
            ]}
            textContentType={'none'}
          />
        </View>
      </View>
    );
  }

  function renderAccessibilityHint(): ReactElement {
    return (
      <View
        accessible={true}
        accessibilityHint={Strings.accessibilityHint}
        accessibilityLabel={`${Strings.accessibilityHint} ${Strings.component}`}
        style={HelperStyles.padding(8, 8)}>
        <Text
          style={HelperStyles.textView(
            16,
            '600',
            Theme.text,
            'center',
            'none',
          )}>
          {Strings.accessibilityHint}
        </Text>
      </View>
    );
  }

  function renderAccessibilityLiveRegion(): ReactElement {
    return (
      <View
        accessible={true}
        accessibilityLabel={`${Strings.accessibilityLiveRegion} ${Strings.component}`}
        style={HelperStyles.padding(4, 4)}>
        <TouchableOpacity
          onPress={(): void => {
            setCount(count + 1);
          }}
          style={HelperStyles.padding(4, 4)}>
          <Text
            accessibilityLiveRegion={'polite'}
            style={HelperStyles.textView(
              16,
              '600',
              Theme.text,
              'center',
              'none',
            )}>
            {Strings.accessibilityLiveRegion} {count}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={HelperStyles.screenSubContainer}>
        {renderAccessible()}

        {renderAccessibilityLabel()}

        {renderAccessibilityLabelledBy()}

        {renderAccessibilityHint()}

        {renderAccessibilityLiveRegion()}
      </View>
    </View>
  );
};

export default Dashboard;
