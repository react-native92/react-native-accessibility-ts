import React, {FC} from 'react';
import {Image, View} from 'react-native';
import Assets from '../../assets/Index';
import Colors from '../../utils/Colors';
import Strings from '../../utils/Strings';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const Indicator: FC = () => {
  // Indicator Variables

  // Theme Variables
  const themeScheme = Helpers.getThemeScheme();

  return (
    <View
      style={[
        HelperStyles.screenContainer(
          themeScheme == Strings.dark ? Colors.black : Colors.white,
        ),
        HelperStyles.justifyContentCenteredView('center'),
      ]}>
      <Image
        resizeMode={'contain'}
        source={Assets.indicator}
        style={HelperStyles.imageView('25%', '25%')}
      />
    </View>
  );
};

export default Indicator;
