import React, {ReactElement, useEffect, useState} from 'react';
import {AppState, Platform, View} from 'react-native';
import * as Helpers from '../../utils/Helpers';

function SecurityIOS(Wrapped: any): ReactElement {
  // SecurityIOS Variables
  const [security, setSecurity] = useState<boolean>(
    Helpers.appStates.includes(AppState.currentState),
  );

  useEffect(() => {
    AppState.addEventListener('change', onChangeAppState);
  }, []);

  function onChangeAppState(appState: any): void {
    const securityStatus: boolean = Helpers.appStates.includes(appState);

    setSecurity(securityStatus);
  }

  return security ? <View /> : <Wrapped />;
}

function SecurityAndroid(Wrapped: any): ReactElement {
  return Wrapped;
}

export const Security = Platform.OS === 'ios' ? SecurityIOS : SecurityAndroid;
