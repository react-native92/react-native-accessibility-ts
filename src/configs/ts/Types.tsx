export type KeyStrValAllType = {[index: string]: any};

export type KeyStrValNumStrType = {[index: string]: number | string};

export type KeyStrValNumType = {[index: string]: number};

export type KeyStrValStrType = {[index: string]: string};
