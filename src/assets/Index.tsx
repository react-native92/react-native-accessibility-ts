const Assets = {
  // Gifs
  indicator: require('./gifs/indicator.gif'),

  // Images
  logo: require('./images/logo.png'),
};

export default Assets;
