import React, {ReactElement} from 'react';
import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack';
import Colors from '../utils/Colors';
import Dashboard from '../screens/appScreens/Dashboard';
import Strings from '../utils/Strings';

const AppNavigation = (): ReactElement => {
  // AppNavigation Variables
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Group
      screenOptions={(): NativeStackNavigationOptions => ({
        statusBarColor: Colors.primary75,
        statusBarStyle: 'light',
      })}>
      <Stack.Screen
        name={Strings.dashboard}
        component={Dashboard}
        options={(): NativeStackNavigationOptions => ({
          headerBackVisible: false,
        })}
      />
    </Stack.Group>
  );
};

export default AppNavigation;
