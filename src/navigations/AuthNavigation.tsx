import React, {ReactElement} from 'react';
import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack';
import Splash from '../screens/authScreens/Splash';
import Strings from '../utils/Strings';

const AuthNavigation = (): ReactElement => {
  // AuthNavigation Variables
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Group
      screenOptions={(): NativeStackNavigationOptions => ({
        headerShown: false,
      })}>
      <Stack.Screen name={Strings.splash} component={Splash} />
    </Stack.Group>
  );
};

export default AuthNavigation;
