import React, {FC} from 'react';
import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack';
import AppNavigation from './AppNavigation';
import AuthNavigation from './AuthNavigation';
import Colors from '../utils/Colors';
import Strings from '../utils/Strings';

const RootNavigation: FC = () => {
  // RootNavigation Variables
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Navigator
      initialRouteName={Strings.splash}
      screenOptions={(): NativeStackNavigationOptions => ({
        headerBackTitleVisible: false,
        headerTintColor: Colors.white,
        headerTitleAlign: 'left',
        headerShadowVisible: false,
        headerStyle: {backgroundColor: Colors.primary},
        headerTitleStyle: {color: Colors.white},
      })}>
      {AuthNavigation()}

      {AppNavigation()}
    </Stack.Navigator>
  );
};

export default RootNavigation;
