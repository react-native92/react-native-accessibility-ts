import {Dimensions, useColorScheme} from 'react-native';
import Strings from './Strings';

// Common Functions

// Common Variables
export const appStates: readonly string[] = [
  Strings.background,
  Strings.inactive,
];

// Get Screen Height
export const screenHeight: number =
  Dimensions.get('screen').height > Dimensions.get('screen').width
    ? Dimensions.get('screen').height
    : Dimensions.get('screen').width;

// Get Screen Weight
export const screenWidth: number =
  Dimensions.get('screen').width > Dimensions.get('screen').height
    ? Dimensions.get('screen').width
    : Dimensions.get('screen').height;

// Get Window Height
export const windowHeight: number =
  Dimensions.get('window').height > Dimensions.get('window').width
    ? Dimensions.get('window').height
    : Dimensions.get('window').width;

// Get Window Weight
export const windowWidth: number =
  Dimensions.get('window').width > Dimensions.get('window').height
    ? Dimensions.get('window').width
    : Dimensions.get('window').height;

// Theme Functions
export function getThemeScheme() {
  return useColorScheme();
}
