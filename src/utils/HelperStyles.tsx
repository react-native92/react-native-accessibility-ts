import {
  KeyStrValNumType,
  KeyStrValStrType,
  KeyStrValNumStrType,
} from '../configs/ts/Types';

// Flex Styles
export function flex(flex: number): KeyStrValNumStrType {
  return {flex};
}

export function flexDirection(flexDirection: string): KeyStrValNumStrType {
  return {flexDirection};
}

// Image Styles
export function imageView(
  height: number | string,
  width: number | string,
): KeyStrValNumStrType {
  return {height, width};
}

// Other Styles
export function alignItemsCenteredView(alignItems: string): KeyStrValStrType {
  return {
    justifyContent: 'center',
    alignItems,
  };
}

export function justifyContentCenteredView(
  justifyContent: string,
): KeyStrValStrType {
  return {
    justifyContent,
    alignItems: 'center',
  };
}

export function justView(
  styleLabel: string,
  styleValue: any,
): KeyStrValNumStrType {
  return {
    [styleLabel]: styleValue,
  };
}

export function margin(
  marginHorizontal: number,
  marginVertical: number,
): KeyStrValNumType {
  return {marginHorizontal, marginVertical};
}

export function padding(
  paddingHorizontal: number,
  paddingVertical: number,
): KeyStrValNumType {
  return {paddingHorizontal, paddingVertical};
}

// Screen Styles
export function screenContainer(backgroundColor: string): KeyStrValNumStrType {
  return {flex: 1, backgroundColor};
}

export const screenSubContainer: KeyStrValNumType = {
  marginHorizontal: 16,
  marginVertical: 16,
};

// Text Styles
export function textView(
  size: number,
  weight: string,
  color: string,
  align: string,
  textTransform: string,
): KeyStrValNumStrType {
  return {
    color,
    fontSize: size,
    fontWeight: weight,
    textAlign: align,
    textTransform: textTransform,
  };
}
