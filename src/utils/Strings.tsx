const Strings = {
  // App Strings
  dark: 'dark',

  // AppNavigation Strings
  dashboard: 'Dashboard',

  // AuthNavigation Strings
  splash: 'Splash',

  // Dashboard Strings
  accessible: 'Accessible',
  accessibilityHint: 'Accessibility Hint',
  accessibilityLabel: 'Accessibility Label',
  accessibilityLabelledBy: 'Accessibility Labelled By',
  accessibilityLiveRegion: 'Accessibility Live Region',
  component: 'Component',
  emailAddress: 'Email Address',
  exitAccount: 'Exit Account',
  exitAlertText: 'Are you sure to exit RN Accessibility app?',
  textInput: 'Text Input',

  // Helpers Strings
  background: 'background',
  inactive: 'inactive',

  // Other Button Strings
  cancel: 'Cancel',
  confirm: 'Confirm',

  // Splash Strings

  // Error Strings
};

export default Strings;
