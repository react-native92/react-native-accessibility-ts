const Colors = {
  // Theme Hex Colors
  primary: '#2ED678',

  // App Hex Colors
  black: '#000000',
  lightGrey: '#D3D3D3',
  manatee: '#9AA1AD',
  red: '#FF0000',
  transparent: 'transparent',
  white: '#FFFFFF',

  // App RGBA Colors
  primary75: 'rgba(46, 214, 120, 0.75)',
};

export default Colors;
