import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const Dashboard = StyleSheet.create({
  screenContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flexDirection: 'row',
    height: 40,
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 1,
    marginVertical: 4,
  },
});

export default Dashboard;
